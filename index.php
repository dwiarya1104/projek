<?php 
session_start();

if (isset($_GET["pesan"])){
    $pesan = $_GET["pesan"];
    } else {
    $pesan = "";
    }

if(isset($_SESSION["login"]) && ($_SESSION["login"])=== 1){
	header("location: home.php");
}
?>

<html>
<head>
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
</head>

<style type="">
	body {
		padding: 0;
		margin: 0;
		font-family: sans-serif;
		background: url(1.jpg);
		background-repeat: no-repeat;
		background-size: cover;
		background-position: absolute;

	}


	.pesan {
		text-align: center;
		color: black;
		font-family: sans-serif;
		font-size: 15px;
		padding-bottom: 10px;
	}


	.logo {
		font-size: 45px;
		text-align: center;
		position: fixed;
		line-height: 60px;
		width: 60px;
		height: 60px;
		top: 13%;
		left: 50%;
		transform: translate(-50%, -50%);
		color: 	black;
		background: #eee;
		border-radius: 50%;
	}

	h1 {
		text-align: center;
		color: 	black;
		padding-top: 40px;
	}

	.login {
		position: absolute;
		left: 50%;
		top: 50%;
		transform: translate(-50%, -50%);
		background-color: #fff;
		box-shadow: 0 0 35px 5px black;
		padding: 20px;
		width: 290px;
	}

	.box-login {
		display: flex;
		border-bottom: 2px solid black;
		margin-bottom: 15px;
		padding: 8px 0;
	}

	.box-login i {
		padding-right: 3px;
		color: black;
	}

	.box-login input {
		background: none;
		outline: none;
		border: none;
		width: 100%;
	}

	.box-login input::placeholder{
		color: black;
		font-style: italic;
	}

	.btn-login {
		width: 100%;
		height: 30px;
		background: none;
		color: black;
		border: 1px solid black;
		
	}

	.btn-login :hover {
		background-color: rgba(0, 0, 0, 0.5);
	}

	.go a {
		text-decoration: none;
		font-style: italic;
		color: #42a7f5;
	}	

	.go a:hover {
		text-decoration: underline;
	}

	h4 {
		color: black;
		font-size: 13.3333px;
		text-align: center;
	}

	button:hover{
		color: white;
		background-color: black;
		cursor: pointer;
	}
</style>

<body>

<div class="login">
	<div class="logo">
		<i class="fas fa-user-alt"></i>
	</div>
		<h1 class="judul">Login</h1>

	<div class="pesan">
		<?php 
			echo $pesan;
		?>
	</div>

	<form method="POST" action="proses_login.php">
		<div class="box-login">
			<i class="fa fa-user"></i>
			<input type="text" name="username" placeholder="username" required><br>
		</div>	

		<div class="box-login">
			<i class="fa fas fa-lock"></i>
			<input type="password" name="password" placeholder="password" required=><br>
		</div>
		<button class="btn-login" type="submit"><strong>Login</strong></button>
	</form>

	<div class="go">
		<h4>Belum punya akun? <a href="registrasi.php"><strong>Register</strong></a></h4>
	</div>
</div>



</body>
</html>