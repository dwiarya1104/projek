<!DOCTYPE html>
<!DOCTYPE html>
<html>
<head>
	<title>Registrasi</title>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
</head>

<style type="text/css">
	body {
		padding: 0;
		margin: 0;
		font-family: sans-serif;
		background: url(1.jpg);
		background-repeat: no-repeat;
		background-size: cover;
		background-position: absolute;

	}

	.logo {
		font-size: 43px;
		text-align: center;
		position: fixed;
		line-height: 60px;
		width: 60px;
		height: 60px;
		top: 9%;
		left: 50%;
		transform: translate(-50%, -50%);
		color: 	black;
		background: #eee;
		border-radius: 55%;
	}

	h1 {
		text-align: center;
		color: 	black;
		padding-top: 30px;
	}
	
	.register {
		position: absolute;
		left: 50%;
		top: 50%;
		transform: translate(-50%, -50%);
		background-color:#fff;
		box-shadow: 0 0 25px 5px black;
		padding: 20px;
		width: 350px;
		border: 1px solid black;
	}

	.box-register {
		display: flex;
		border-bottom: 2px solid black;
		margin-bottom: 15px;
		padding: 8px 0;
	}

	.box-register i {
		padding-right: 3px;
		color: black;
	}

	.box-register input {
		background: none;
		outline: none;
		border: none;
		width: 100%;
	}

	.box-register input::placeholder{
		color: black;
		font-style: italic;	
		font-size: 	15px;
	}

	.btn-register button {
		width: 100%;
		height: 30px;
		background: none;
		color: black;
		border: 1px solid black;
		
	}

	a {
		text-decoration: none;
		font-style: italic;
		color: #42a7f5;
	}

	a:hover{
		text-decoration: underline;
	}	

	h4{
		font-size: 13.3333px;
		text-align: center;
	}

	button:hover{
		color: white;
		background-color: black;
		cursor: pointer;
	}
</style>

<body>
<div class="register">
	<div class="logo">
		<i class="fas fa-user-plus"></i>
	</div>
<h1>Registrasi</h1>
	<form action="proses_register.php" method="POST" >
		<div class="box-register">
			<i class="fas fa-pen"></i>
			<input type="text" name="name" placeholder="nama lengkap" required>
		</div>
		
		<div class="box-register">
			<i class="fa fa-user"></i>
			<input type="text" name="username" placeholder="username" required>
		</div>
		
		<div class="box-register">
			<i class="fa fa-envelope"></i>
			<input type="email" name="email" placeholder="email" required>
		</div>

		<div class="box-register">
			<i class="fas fa-key"></i>
			<input type="password" name="password" placeholder="password" required>
		</div>

		<div class="box-register">
			<i class="fa fas fa-lock"></i>
			<input type="password" name="konfirmasi_password" placeholder="konfirmasi password" required>
		</div>


		<div class="btn-register">
			<button type="submit" ><strong>Register!</strong></button><br>
		</div>

		<h4>Sudah memiliki akun? <a type="button" href="index.php"><strong>Login</strong></a></h4>			
	</form>
</div>
































<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
</body>
</html>
