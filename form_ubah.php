<?php 

include 'koneksi.php';

$id = $_GET["id"];

$sql = "SELECT * FROM users WHERE id='$id'";
$result = $koneksi -> query($sql);
$hasil = $result -> fetch_assoc();	

 ?>


<!DOCTYPE html>
<html>
<head>
	<title>Ubah Data</title>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
</head>

<style>
	body {
		padding: 0;
		margin: 0;
		font-family: sans-serif;
		background: url(1.jpg);
		background-repeat: no-repeat;
		background-size: cover;
		background-position: absolute;

	}

	.logo {
		font-size: 43px;
		text-align: center;
		position: fixed;
		line-height: 60px;
		width: 60px;
		height: 60px;
		top: 9%;
		left: 50%;
		transform: translate(-50%, -50%);
		color: 	black;
		background: #eee;
		border-radius: 55%;
	}

	h1 {
		text-align: center;
		color: 	black;
		padding-top: 30px;
	}
	
	.ubah {
		position: absolute;
		left: 50%;
		top: 50%;
		transform: translate(-50%, -50%);
		background-color:#fff;
		box-shadow: 0 0 25px 5px black;
		padding: 20px;
		width: 290px;
		border: 1px solid black;
	}

	.box-ubah {
		display: flex;
		border-bottom: 2px solid black;
		margin-bottom: 15px;
		padding: 8px 0;
	}

	.box-ubah i {
		padding-right: 3px;
		color: black;
	}

	.box-ubah input {
		background: none;
		outline: none;
		border: none;
		width: 100%;
	}

	.box-ubah input::placeholder{
		color: black;
		font-style: italic;	
		font-size: 	15px;
	}

	.btn-ubah button {
		width: 100%;
		height: 30px;
		background: none;
		color: black;
		border: 1px solid black;
		
	}

	a {
		text-decoration: none;
		font-style: italic;
		color: #42a7f5;
	}

	a:hover{
		text-decoration: underline;
	}	

	h4{
		font-size: 13.3333px;
	}

	button:hover{
		color: white;
		background-color: black;
		cursor: pointer;
	}
</style>
</style>

<body>

<div class="ubah">
	<div class="logo">
		<i class="fas fa-user-edit"></i>
	</div>
<h1>Ubah Data</h1>
	<form action="proses_ubah.php" method="POST" >
		<input type="hidden" name="id" value="<?="$id"?>">

		<div class="box-ubah">
			<i class="fas fa-pen"></i>
			<input value="<?= $hasil["name"] ?>" type="text" name="name" placeholder="nama lengkap" required>
		</div>
		
		<div class="box-ubah">
			<i class="fa fa-user"></i>
			<input value="<?= $hasil["username"] ?>" type="text" name="username" placeholder="username" required>
		</div>
		
		<div class="box-ubah">
			<i class="fa fa-envelope"></i>
			<input value="<?= $hasil["email"] ?>" type="email" name="email" placeholder="email" required>
		</div>

		<div class="btn-ubah">
			<button type="submit"><strong>Kirim!</strong></button>
		</div>
		<h4>Tidak Jadi Ubah?<a type="button" href="home.php#akun"><strong>Batal</strong></a></h4>
	</form>


</body>
</html>