<?php  
session_start();
include 'koneksi.php';

if (!isset($_SESSION["login"])) {
      header("location: index.php");
      exit;
  }


if (isset($_GET["pesan"])) {
  //PESAN HAPUS//
  if ($_GET["pesan"]=== "berhasil_hapus") {
    $pesan = "Berhasil Menghapus Data";
    $warna = "success";
  }
  if ($_GET["pesan"]=== "gagal_hapus") {
    $pesan = "Gagal Menghapus Data";
    $warna = "danger";
  }
  //PESAN UBAH//
  if ($_GET["pesan"]=== "berhasil_ubah") {
    $pesan = "Berhasil Mengubah Data";
    $warna = "success";
  }
  if ($_GET["pesan"]=== "gagal_ubah") {
    $pesan = "Gagal Mengubah Data";
    $warna = "danger";
  }
}

$username = $_SESSION['username'];

$sql = "SELECT * FROM users";
$result = mysqli_query($koneksi, $sql);

// Fetch all
$hasil = mysqli_fetch_all($result, MYSQLI_ASSOC);

?>


<!DOCTYPE html>
<html>
<head>
	<title>My Blog</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
</head>

<style type="text/css">
	* {
		font-family: Arial;
	}

	body {
		background: url(3.jpg);
		background-repeat: no-repeat;
		background-size: cover;
	}

	h1{
		color: white;
		font-family: Arial;
		padding: 250px 150px 250px 180px;
		font-size: 50px;
	}

	.footer {
    position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    background-color: #212529;
    color: white;
    text-align: center;
    padding: 15px 0px 0px 15px;
    font-size: 20px;
    }

    .datauser i{
    text-align: center;
    font-size: 150px;
    margin-left: 44.5%;
    padding-top: 60px; 
    }



    h3 {
        text-align: center;
        padding-top: px;
        text-decoration: underline;
        padding-bottom: 0;
    }

    #akun{
      background-color: #b3b3b3;
      padding-bottom: 130px;
    }

    table {
        border: 2px solid black; 
    }


    td{
        padding: 10px;
    }

    td a i {
      text-align: center;
    }
</style>

<body>


<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top ">
	<div class="container">
    	<a class="navbar-brand" href="#">Dwi Website</a>
    	<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      	<span class="navbar-toggler-icon"></span>
    	</button>
    		<div class="collapse navbar-collapse" id="navbarNav">
      			<ul class="navbar-nav">
        			<li class="nav-item">
          				<a class="nav-link" aria-current="page" href="#">Home</a>
        			</li>
              <li class="nav-item">
                  <a class="nav-link" href="#akun">Account</a>
              </li>
      			</ul>
    		</div>
                
                <div class="ml-auto navbar-nav">
                    <a type="button"style="width:110px;" class="btn btn-secondary " href="logout.php">Log out</a>
                </div>
  	</div>
</nav>
    <!-- Navbar End-->

<h1>WELCOME TO MY WEBSITE</h1>

<div id="akun">
<div class="container">
  <div class="datauser">
    <i class="fas fa-user-alt"></i>
    <h3>Data User</h3>
  </div>

  <?php 
  if (isset($pesan)) {
    ?>
    <div class="alert alert-<?= $warna?> alert-dissmissible">
      <?= $pesan ?>
    </div>
  <?php  
  
  }
   
   ?>

<table class="table table-hover">
    <thead>
        <tr>
          <th>No.</th>
          <th>Nama</th>
          <th>Username</th>
          <th>Email</th>
          <th>Avatar</th>
          <th>Action</th>
        </tr>
    </thead>
    
    <tbody>
      <?php 
      foreach ($hasil as $key => $user_data) {
        ?>
        <tr>
          <td><?= $key + 1 ?></td>
          <td><?= $user_data["name"]?></td>
          <td><?= $user_data["username"] ?></td>
          <td><?= $user_data["email"] ?></td>
          <td><?= $user_data["avatar"] ?></td>
          <td>
            <a class="btn btn-success" href="form_ubah.php?id=<?=$user_data['id']?>"><i class="fas fa-pen"></i></a>
            <a class="btn btn-danger" href="proses_hapus.php?id=<?=$user_data['id']?>"><i class="fas fa-trash"></i></a>
          </td>
        </tr>
       <?php 
      }
      ?>
    </tbody>
</table>
    <a class="btn btn-primary" href="registrasi.php" style="float: right;">
      <i class="fas fa-user-plus">Tambah Data</i>
    </a>
</div>
</div>
    <!-- Akun -->


<!-- Footer -->
<div class="footer">
	<p>&copy; Copyright by Dwi Arya Putra - 2021</p>
</div>
<!-- Tutup Footer -->




<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>

</body>
</html>